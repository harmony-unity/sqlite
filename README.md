# SQLite Package

This is the Harmony Unity package containing the SQLite wrapper.

## Updating dependencies

Go to the [SQLite Website](https://www.sqlite.org/index.html) and indentify the lastest version number. Then, replace 
the last digits of these two URLs with that version number, without the dots. For example, using version `3.25.2`,
you get `3250200`. Also, make sure you replace the year in the URLs.

```
https://www.sqlite.org/2018/sqlite-dll-win32-x86-3250200.zip

https://www.sqlite.org/2018/sqlite-dll-win64-x64-3250200.zip
```

Take the two DLLs and place them in their respective folders (see `Assets/Libraries/SqLite/Plugins`).
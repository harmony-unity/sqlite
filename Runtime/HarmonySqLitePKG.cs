﻿using System.IO;

namespace Harmony
{
    public static class HarmonySqLitePkg
    {
        public static readonly string PackagePath = Path.GetFullPath("Packages/ca.harmony.sqlite");
#if UNITY_EDITOR_WIN
        public static readonly string BrowserPath = Path.Combine(PackagePath, "Binairies/SQLiteDBBrowser/SQLiteDBBrowser.exe");
#endif
        public static readonly string EmptyDBPath = Path.Combine(PackagePath, "Binairies/SQLiteDBBrowser/Empty.db");
    }
}
﻿using System;
using System.Data.Common;
using System.IO;
using Mono.Data.Sqlite;
using UnityEngine;

namespace Harmony
{
    /// <summary>
    /// SqLite DbConnection factory.
    /// </summary>
    public class SqLiteConnectionFactory : MonoBehaviour 
    {
        private const string SqliteConnectionTemplate = "URI=file:{0}";

        [SerializeField] private string databaseFileName = "Database.db";
        
        /// <summary>
        /// Path to the database file in the user home folder.
        /// </summary>
        public string CurrentDatabaseFilePath => Path.Combine(Application.persistentDataPath, databaseFileName);

        /// <summary>
        /// Does the the database exists in the user home folder.
        /// </summary>
        public bool CurrentDatabaseExists => File.Exists(CurrentDatabaseFilePath);

        /// <summary>
        /// Path to the database file in the project assets.
        /// </summary>
        public string SourceDatabaseFilePath => Path.Combine(Application.streamingAssetsPath, databaseFileName);

        /// <summary>
        /// Does the database exists in the project assets.
        /// </summary>
        public bool SourceDatabaseExists => File.Exists(SourceDatabaseFilePath);
        
        /// <summary>
        /// Connection string to the database in the user home folder.
        /// </summary>
        private string ConnexionString => string.Format(SqliteConnectionTemplate, CurrentDatabaseFilePath);

        /// <summary>
        /// Gets a new DbConnection for the configured database.
        /// </summary>
        [Obsolete]
        public DbConnection GetConnection()
        {
            CreateDatabaseIfNotExits();

            return new SqliteConnection(ConnexionString);
        }
        
        /// <summary>
        /// Gets a new DbConnection for the configured database.
        /// </summary>
        public DbConnection OpenConnection()
        {
            CreateDatabaseIfNotExits();

            var connection = new SqliteConnection(ConnexionString);
            connection.Open();
            return connection;
        }

        /// <summary>
        /// Copies the database file in the projects assets to the user home folder if it does not already exits.
        /// </summary>
        public void CreateDatabaseIfNotExits()
        {
            if (!CurrentDatabaseExists)
            {
                File.Copy(SourceDatabaseFilePath, CurrentDatabaseFilePath, true);
            }
        }

        /// <summary>
        /// Copies the database file in the projects assets to the user home folder.
        /// </summary>
        public void ResetDatabase()
        {
            if (CurrentDatabaseExists)
            {
                File.Delete(CurrentDatabaseFilePath);
            }
        }
    }
}
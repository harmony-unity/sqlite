#if UNITY_EDITOR_WIN
using System.Diagnostics;
#endif
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Harmony
{
    [CustomEditor(typeof(SqLiteConnectionFactory), true)]
    public class SqLiteConnectionFactoryInspector : BaseInspector
    {
        private SqLiteConnectionFactory sqLiteConnectionFactory;

        protected override void Initialize()
        {
            sqLiteConnectionFactory = target as SqLiteConnectionFactory;
        }

        protected override void Draw()
        {
            Initialize();
            DrawDefault();

            if (sqLiteConnectionFactory.SourceDatabaseExists)
            {
                sqLiteConnectionFactory.CreateDatabaseIfNotExits();

                BeginTable();
                BeginTableCol();
                DrawTitleLabel("Database utils");
#if UNITY_EDITOR_WIN
                DrawLabel("Open the source database file of the project.");
                DrawButton("Open Source Database", OpenSourceDatabase);
                DrawLabel("Open the current database file of the user.");
                DrawButton("Open Current Database", OpenCurrentDatabase);
#endif
                DrawLabel("Reset the current database file of the user.");
                DrawButton("Reset Current Database", ResetCurrentDatabase);
                EndTableCol();
                EndTable();
            }
            else
            {
                DrawErrorBox("Database doesn't exists. " +
                             "Make sure there's a database in the \"StreamingAssets\" folder. You can also create it.");
                DrawButton("Create Source Database", CreateSourceDatabase);
            }
        }

#if UNITY_EDITOR_WIN
        private void OpenSourceDatabase()
        {
            OpenSourceDatabase(sqLiteConnectionFactory.SourceDatabaseFilePath);
        }

        private void OpenCurrentDatabase()
        {
            sqLiteConnectionFactory.CreateDatabaseIfNotExits();

            OpenSourceDatabase(sqLiteConnectionFactory.CurrentDatabaseFilePath);
        }
#endif

        private void ResetCurrentDatabase()
        {
            sqLiteConnectionFactory.ResetDatabase();
        }

        private void CreateSourceDatabase()
        {
            CreateSourceDatabase(sqLiteConnectionFactory.SourceDatabaseFilePath);
        }

#if UNITY_EDITOR_WIN
        private void OpenSourceDatabase(string databasePath)
        {
            var pathToDbBrowser = HarmonySqLitePkg.BrowserPath;
            var processStartInfo = new ProcessStartInfo
            {
                FileName = pathToDbBrowser, 
                Arguments = "\"" + databasePath + "\""
            };
            Process.Start(processStartInfo);
        }
#endif

        private void CreateSourceDatabase(string databasePath)
        {
            var pathToEmpty = HarmonySqLitePkg.EmptyDBPath;
            var pathToNew = Path.GetFullPath(databasePath);

            var streamingAssetsFolder = Path.GetFullPath(Application.streamingAssetsPath);
            if (!Directory.Exists(streamingAssetsFolder)) Directory.CreateDirectory(streamingAssetsFolder);
            File.Copy(pathToEmpty, pathToNew);

            AssetDatabase.Refresh();
        }
    }
}